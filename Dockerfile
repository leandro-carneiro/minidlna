FROM vladgh/minidlna
ADD entrypoint.sh /
ENV MINIDLNA_MEDIA_DIR "/media"
ENV MINIDLNA_FRIENDLY_NAME "MiniDLNA"
VOLUME ["/media","/var/lib/minidlna"]
EXPOSE 8200 1900 1900/udp
