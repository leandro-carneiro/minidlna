```
docker run -d --net=host \
  --name=minidlna \
  -v /media:/media \
  -v /opt/minidlna:/var/lib/minidlna \
  leandrocarneiro/minidlna
```
